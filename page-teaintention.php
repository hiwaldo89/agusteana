<?php
/**
 * Template Name: TeaIntention
 */
get_header(); ?>
    <div class="py-5">
        <div class="container">
            <div class="row">
                <div class="col-lg-9 mx-lg-auto">
                    <?php if(have_rows('images')) : while(have_rows('images')) : the_row(); ?>
                        <img src="<?php the_sub_field('image'); ?>" alt="" class="img-fluid d-block mx-auto mb-4">
                    <?php endwhile; endif; ?>
                </div>
            </div>
        </div>
    </div>
<?php get_footer(); ?>