<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <?php gravity_form_enqueue_scripts( 1 ); ?>
    <?php wp_head(); ?>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-53439952-17"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-53439952-17');
    </script>
</head>
<body <?php body_class(); ?>>
    <h1 class="sr-only"><?php bloginfo( 'name' ); ?></h1>
    <div class="main-header py-4">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-6">
                    <a href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php bloginfo('template_url'); ?>/src/images/logo.svg" alt="Agusteana Bar" class="logo"></a>
                </div>
                <div class="col-6 text-right">
                    <a href="<?php echo get_permalink( get_page_by_path( 'eventos' ) ); ?>" class="agusteana-btn">Cotiza tu evento</a>
                </div>
            </div>
        </div>
    </div>