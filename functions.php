<?php

function agusteana_setup() {
    load_theme_textdomain( 'agusteana', get_template_directory() . '/languages' );
    add_theme_support( 'automatic-feed-links' );
    add_theme_support( 'title-tag' );
    add_theme_support( 'post-thumbnails' );
    add_theme_support( 'html5', array(
        'search-form',
        'comment-form',
        'comment-list',
        'gallery',
        'caption',
    ) );
}
add_action( 'after_setup_theme', 'agusteana_setup' );

function agusteana_scripts() {
    wp_enqueue_style( 'google-fonts', 'https://fonts.googleapis.com/css?family=Cormorant:500,500i,600&display=swap' );
    wp_enqueue_style( 'agusteana-style', get_stylesheet_uri() );

    wp_enqueue_script( 'agusteana-js', get_template_directory_uri() . '/dist/bundle.js', array('jquery'), '', true );
}
add_action('wp_enqueue_scripts', 'agusteana_scripts');

// GravityForms compatibility file
require_once get_template_directory() . '/inc/agusteana-gravityforms.php';