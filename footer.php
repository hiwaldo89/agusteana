    <footer class="agusteana-footer py-5">
        <div class="footer__icon">
            <div class="icon-wrapper"><img src="<?php bloginfo('template_url'); ?>/src/images/tea-icon.svg" alt=""></div>
        </div>
        <ul class="social-menu">
            <li>
                <a href="https://www.instagram.com/agusteanabar/" target="_blank">
                    <span class="d-none d-md-block">Instagram</span>
                    <i class="fab fa-instagram d-md-none"></i>
                </a>
            </li>
            <li class="mx-3 mx-md-0">
                <a href="https://www.facebook.com/Agusteana-Bar-1078663652339515/" target="_blank">
                    <span class="d-none d-md-block">Facebook</span>
                    <i class="fab fa-facebook-f d-md-none"></i>
                </a>
            </li>
            <li>
                <a href="mailto:eventos@agusteanabar.com">
                    <span class="d-none d-md-block">Correo</span>
                    <i class="far fa-envelope d-md-none"></i>
                </a>
            </li>
        </ul>
        <a href="tel:4427088531" class="color-dark d-block text-center mt-5">442 708 85 31</a>
        <a href="" class="color-dark d-block text-center">eventos@agusteanabar.com</a>
    </footer>
    <?php wp_footer(); ?>
</body>
</html>