<?php

if ( class_exists( 'GFCommon' ) ) {

    // filter the Gravity Forms button type
    add_filter( 'gform_submit_button', 'form_submit_button', 10, 2 );
    function form_submit_button( $button, $form ) {
        // return "<button class='btn btn-primary animated' id='gform_submit_button_{$form['id']}'>" . $form['button']['text'] . "</button>";
        $dom = new DOMDocument();
        $dom->loadHTML( $button );
        $input = $dom->getElementsByTagName( 'input' )->item(0);
        $new_button = $dom->createElement( 'button' );
        $new_button->appendChild( $dom->createTextNode( $input->getAttribute( 'value' ) ) );
        $input->removeAttribute( 'value' );
        foreach( $input->attributes as $attribute ) {
            $new_button->setAttribute( $attribute->name, $attribute->value );
        }
        $input->parentNode->replaceChild( $new_button, $input );

        $new_button->setAttribute( 'class', "agusteana-btn" );

        return $dom->saveHtml( $new_button );
    }

}