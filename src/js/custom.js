import AOS from "aos";
import Instafeed from "instafeed.js";

AOS.init({
  once: true,
  delay: 300
});

const feed = new Instafeed({
  userId: "13350637636",
  accessToken: "13350637636.53ebe38.3686dd21bc444429b33b8df94eb9839a",
  get: "user",
  limit: 3,
  resolution: "standard_resolution",
  template:
    "<div class='col-4'><a href='{{link}}' target='_blank'><div class='box bg-cover' style='background-image: url({{image}});'></div></a></div>"
});
feed.run();

gform.addFilter("gform_datepicker_options_pre_init", function(
  optionsObj,
  formId,
  fieldId
) {
  optionsObj.changeMonth = false;
  optionsObj.changeYear = false;
  optionsObj.minDate = 0;
  return optionsObj;
});

(function($) {
  $(".events-slider").slick({
    centerMode: true,
    centerPadding: "250px",
    slidesToShow: 1,
    autoplay: true,
    prevArrow:
      '<button type="button" class="slick-prev"><span><i class="fal fa-chevron-left"></i></span></button>',
    nextArrow:
      '<button type="button" class="slick-next"><span><i class="fal fa-chevron-right"></i></span></button>',
    responsive: [
      {
        breakpoint: 768,
        settings: {
          centerPadding: "30px",
          appendArrows: $(".slider-arrows")
        }
      }
    ]
  });
})(jQuery);
