import "./sass/style.scss";

import "popper.js";
import "bootstrap";
import "slick-carousel";
import "@fortawesome/fontawesome-pro/js/all";
import "./js/custom";
