<?php
/**
 * Template Name: Home
 */
get_header(); ?>
    <div class="welcome-section py-lg-5 mb-lg-5">
        <?php $welcomeSection = get_field('welcome_section'); ?>
        <div class="welcome-section__bg bg-cover" style="background-image: url(<?php echo $welcomeSection['background_image']['url']; ?>);" data-aos="fade-left"></div>
        <div class="container py-5">
            <div class="row">
                <div class="col-lg-9 mx-auto">
                    <div class="p-lg-5 bg-pink text-center welcome-section__content" data-aos="fade-up">
                        <div class="welcome-section__circle bg-cover d-none d-lg-block" style="background-image: url(<?php echo $welcomeSection['circle_image']['url']; ?>);"></div>
                        <div class="py-5 mt-5 mb-lg-5 px-4">
                            <h2 class="h3 font-italic color-dark mb-5"><?php echo $welcomeSection['title']; ?></h2>
                            <div class="px-lg-5 mb-5">
                                <?php echo $welcomeSection['content']; ?>
                            </div>
                            <a href="<?php echo get_permalink( get_page_by_path( 'eventos' ) ); ?>" class="agusteana-btn">Eventos</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <section class="product-section py-5 mb-5">
        <?php if(have_rows('service_blocks')) : while(have_rows('service_blocks')) : the_row(); ?>
            <div class="product-section__item">
                <div class="container mt-5">
                    <div class="row align-items-center">
                        <div class="col-lg-6 ml-lg-auto<?php if(get_row_index() % 2 === 0) : ?> order-lg-1<?php endif; ?>">
                            <div class="product-section__img">
                                <span data-aos="fade-up"></span>
                                <img src="<?php echo get_sub_field('image')['url']; ?>" alt="Infusiones calientes" data-aos="<?php if(get_row_index() % 2 === 0 ) : ?>fade-left<?php else : ?>fade-right<?php endif; ?>">
                            </div>
                        </div>
                        <div class="col-lg-5 mt-5 mt-lg-0<?php if(get_row_index() % 2 === 0) : ?> order-lg-0<?php endif; ?>">
                            <div class="text-center" data-aos="<?php if(get_row_index() % 2 === 0 ) : ?>fade-right<?php else : ?>fade-left<?php endif; ?>">
                                <h2 class="h4 color-dark font-italic mb-5"><?php the_sub_field('title'); ?></h2>
                                <?php the_sub_field('content'); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php endwhile; endif; ?>
    </section>
<?php get_footer(); ?>