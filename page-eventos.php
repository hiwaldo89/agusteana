<?php
/**
 * Template Name: Events
 */
get_header(); ?>
    <div class="py-5">
        <div class="container">
            <div class="row">
                <div class="col-lg-7 mx-lg-auto">
                    <h2 class="h3 font-italic color-dark mb-5 text-center"><?php the_title(); ?></h2>
                    <div class="text-center">
                        <?php the_field('contact_text'); ?>
                    </div>
                    <?php gravity_form(1, false, false, false, false); ?>
                </div>
            </div>
        </div>
    </div>
    <div class="events-slider">
        <?php $images = get_field('galeria'); ?>
        <?php foreach( $images as $image ) : ?>
            <div class="events-slider__slide test">
                <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" class="img-fluid" data-no-lazy="1">
            </div>
        <?php endforeach; ?>
    </div>
    <div class="slider-arrows mb-5 pb-5 mb-lg-0 pb-lg-0"></div>
    <div class="social-section py-5 mb-5 d-none d-lg-block">
        <div class="container">
            <div class="row">
                <div class="col-lg-7 d-flex flex-column justify-content-center">
                    <div id="instafeed" class="row"></div>
                </div>
                <div class="col-lg-1">
                    <span class="divider"></span>
                </div>
                <div class="col-lg-4">
                    <ul class="list-unstyled">
                        <li class="mb-4"><a href="" class="color-dark">Instagram</a></li>
                        <li class="mb-4"><a href="" class="color-dark">Facebook</a></li>
                        <li class="mb-4"><a href="" class="color-dark">Correo</a></li>
                        <li class="mb-4"><a href="" class="color-dark">442 708 85 31</a></li>
                        <li><a href="" class="color-dark">eventos@agusteanabar.com</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
<?php get_footer(); ?>